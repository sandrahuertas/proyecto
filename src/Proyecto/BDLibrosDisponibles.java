/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Proyecto;
/**
 *
 * @author 
 */

import com.mysql.jdbc.jdbc2.optional.SuspendableXAConnection;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class BDLibrosDisponibles {
    
     Connection conexion = null;
    
    public String db;
    public String url;
    public String user;
    public String pass;
    public String driver;

public void conectar(){
        try{
            
            Properties mispropiedades = obtenerProperties();
            
            user = mispropiedades.getProperty("usuario");
            pass = mispropiedades.getProperty("password");
            db = mispropiedades.getProperty("db");
            url = (mispropiedades.getProperty("url"));
            driver = mispropiedades.getProperty("driver");
            
           
            Class.forName(driver);
            
            
            conexion = DriverManager.getConnection(url,user,pass);
        }
        catch(ClassNotFoundException | SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getStackTrace());
        }
}   

    
public Properties obtenerProperties() {
        try {
            
            Properties propiedades = new Properties();
           
            propiedades.load( getClass().getResourceAsStream("datos1.properties") );
           
            if (!propiedades.isEmpty()) {                
                return propiedades;
            } else {
                return null;
            }
        } catch (IOException ex) {
            return null;
        }
   }
public void añadirLibro (String titulo, String genero, String autor) throws SQLException
    {
        Connection ccn = null;
        int resultado=0;
        Statement stmt1;
       try 
        {
            conectar();
            ccn = DriverManager.getConnection("jdbc:mysql://127.0.0.1/libreria", "root", "");

            stmt1 = ccn.createStatement();
           //consulta para añadir los librosnuevos del boton añadir
            String comando = "INSERT INTO .todoslibros (titulo, genero, autor) VALUES ("
                        +"'"+titulo+"',"
                        +"'"+genero+"',"
                        +"'"+autor+"'"+
                        ")";
            stmt1.executeUpdate(comando);   
            
            ccn.close();
            
       conexion.close();
        } 
       catch (SQLException ex) 
       {
           Logger.getLogger(Proyecto.class.getName()).log(Level.SEVERE, null, ex);
       }
        
    }
public void importar (String titulo, String genero, String autor) throws SQLException
    {
        Connection ccn = null;
        Statement stmt1=null;
        try 
        {
            ccn = DriverManager.getConnection("jdbc:mysql://127.0.0.1/libreria", "root", "");
            
            stmt1 = ccn.createStatement();
            String comando="INSERT INTO .todoslibros (titulo, genero, autor) VALUES ("
                        +"'"+titulo+"',"
                        +"'"+genero+"',"
                        +"'"+autor+"'"+
                        ")";
            stmt1.executeUpdate(comando);   
            
        } 
       catch (SQLException ex) 
       {
           Logger.getLogger(Proyecto.class.getName()).log(Level.SEVERE, null, ex);
       }
stmt1.close();
ccn.close();
    }

 public ArrayList<Object[]> listadoCienciaF () throws SQLException
    {
       ArrayList<Object[]> datos = new ArrayList();
        
        try {
            // CONECTAR A LA BASE DE DATOS
            conectar();
            
            // REALIZAR LA CONSULTA
            Statement consulta = conexion.createStatement();
            ResultSet rs =consulta.executeQuery("SELECT titulo, autor from todoslibros where genero='Ciencia-Ficcion'");

            // RECORRER EL RESULTADO
            while(rs.next()){
                Object [] fila=new Object[3];//Crea un vector
                //para almacenar los valores del ResultSet
                fila[0]=(rs.getString("titulo")); 
                fila[1]=(rs.getString("autor")); 
                
      
                // A?ADIR datos de la fila al ArrayList        
                datos.add(fila);  
            }
            rs.close(); //Cierra el ResultSet
            
        } catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getStackTrace());
        }
        
        
        
        conexion.close(); // cierra la conexion con la base de datos
        
        
        
        // Devuelve el array bidimensional para realizar el listado en la Interfaz gráfica.
        return datos;
}
 public ArrayList<Object[]> listadoMisterio () throws SQLException
    {
       ArrayList<Object[]> datos = new ArrayList();
        
        try {
            // CONECTAR A LA BASE DE DATOS
            conectar();
            
            // REALIZAR LA CONSULTA
            Statement consulta = conexion.createStatement();
            ResultSet rs =consulta.executeQuery("SELECT titulo, autor from todoslibros where genero='Misterio y Terror'");

            // RECORRER EL RESULTADO
            while(rs.next()){
                Object [] fila=new Object[3];//Crea un vector
                //para almacenar los valores del ResultSet
                fila[0]=(rs.getString("titulo")); 
                fila[1]=(rs.getString("autor")); 
                
      
                // A?ADIR datos de la fila al ArrayList        
                datos.add(fila);  
            }
            rs.close(); //Cierra el ResultSet
            
        } catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getStackTrace());
        }
        
        
        
        conexion.close(); // cierra la conexion con la base de datos
        
        
        
        // Devuelve el array bidimensional para realizar el listado en la Interfaz gráfica.
        return datos;
}
 public ArrayList<Object[]> listadoDrama () throws SQLException
    {
       ArrayList<Object[]> datos = new ArrayList();
        
        try {
            // CONECTAR A LA BASE DE DATOS
            conectar();
            
            // REALIZAR LA CONSULTA
            Statement consulta = conexion.createStatement();
            ResultSet rs =consulta.executeQuery("SELECT titulo, autor from todoslibros where genero='Drama'");

            // RECORRER EL RESULTADO
            while(rs.next()){
                Object [] fila=new Object[3];//Crea un vector
                //para almacenar los valores del ResultSet
                fila[0]=(rs.getString("titulo")); 
                fila[1]=(rs.getString("autor")); 
                
      
                // A?ADIR datos de la fila al ArrayList        
                datos.add(fila);  
            }
            rs.close(); //Cierra el ResultSet
            
        } catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getStackTrace());
        }
        
        
        
        conexion.close(); // cierra la conexion con la base de datos
        
        
        
        // Devuelve el array bidimensional para realizar el listado en la Interfaz gráfica.
        return datos;
}
 public ArrayList<Object[]> listadoAventuras () throws SQLException
    {
       ArrayList<Object[]> datos = new ArrayList();
        
        try {
            // CONECTAR A LA BASE DE DATOS
            conectar();
            
            // REALIZAR LA CONSULTA
            Statement consulta = conexion.createStatement();
            ResultSet rs =consulta.executeQuery("SELECT titulo, autor from todoslibros where genero='Aventuras'");

            // RECORRER EL RESULTADO
            while(rs.next()){
                Object [] fila=new Object[3];//Crea un vector
                //para almacenar los valores del ResultSet
                fila[0]=(rs.getString("titulo")); 
                fila[1]=(rs.getString("autor")); 
                
      
                // A?ADIR datos de la fila al ArrayList        
                datos.add(fila);  
            }
            rs.close(); //Cierra el ResultSet
            
        } catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getStackTrace());
        }
        
        
        
        conexion.close(); // cierra la conexion con la base de datos
        
        
        
        // Devuelve el array bidimensional para realizar el listado en la Interfaz gráfica.
        return datos;
}
 public ArrayList<Object[]> listadoHumor () throws SQLException
    {
       ArrayList<Object[]> datos = new ArrayList();
        
        try {
            // CONECTAR A LA BASE DE DATOS
            conectar();
            
            // REALIZAR LA CONSULTA
            Statement consulta = conexion.createStatement();
            ResultSet rs =consulta.executeQuery("SELECT titulo, autor from todoslibros where genero='Humor'");

            // RECORRER EL RESULTADO
            while(rs.next()){
                Object [] fila=new Object[3];//Crea un vector
                //para almacenar los valores del ResultSet
                fila[0]=(rs.getString("titulo")); 
                fila[1]=(rs.getString("autor")); 
                
      
                // A?ADIR datos de la fila al ArrayList        
                datos.add(fila);  
            }
            rs.close(); //Cierra el ResultSet
            
        } catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getStackTrace());
        }
        
        
        
        conexion.close(); // cierra la conexion con la base de datos
        
        
        
        // Devuelve el array bidimensional para realizar el listado en la Interfaz gráfica.
        return datos;
}
 //realizar la consulta del boton buscar
 public  ArrayList<Object[]> realizarConsulta(String consultaSql)
    {
        ArrayList<Object[]> datos1 = new ArrayList();
        
        try {
          
            conectar();
            
            
            Statement consulta = conexion.createStatement();
            ResultSet rs = consulta.executeQuery(consultaSql);
                        
            
           
            while(rs.next()){
                Object [] fila=new Object[2];
               
        //mensaje que aparece con los dos elementos de la tabla Titulo y Autor        
                JOptionPane.showMessageDialog(null, rs.getString(1)+"\n"+(rs.getString(2)));
                                 
                datos1.add(fila);    
            }
            rs.close();
            
        } catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getStackTrace());
        }
        
        
        return datos1;
}

    
}


    
    

