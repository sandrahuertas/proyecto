/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Proyecto;

/**
 *
 * @author Sandra Huertas Melero
 */
import javax.swing.*;
import java.awt.*; 
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.Window.*;
import java.awt.Event.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

//clase principal
public class Proyecto extends JFrame{ 
    Container panel;  //agrupamos componenetes en un contenedor invisible
    JFileChooser selector;//clase que nos permite mostrar una ventana para seleccionar un fichero
    File fichero;//fichero
    JTable tabla;
    /*distintos botones de la ventana principal*/
    String Texto;
    JButton bguardar, bimportar;//boton para guardar e importar
    JButton estanteria1,estanteria2,estanteria3,estanteria4,estanteria5,botonbuscar, botonAñadir, bnuevo;
    JTextField tituloLi, titulo, autor;//campo de texto para escribir
    
    
    //nuevo objeto de la clase LibrosDisponibles que nos permite 
    //que aparezca la ventana emergente al pulsar los botones
     LibrosDisponibles ventana1= new LibrosDisponibles();
     LibrosDisponibles ventana2= new LibrosDisponibles();
     LibrosDisponibles ventana3= new LibrosDisponibles();
     LibrosDisponibles ventana4= new LibrosDisponibles();
     LibrosDisponibles ventana5= new LibrosDisponibles();
     JMenuBar jmb1;//BARRA DE MENU
     
     //nuevo objeto de la clase BDLibrosDisponibles
     BDLibrosDisponibles bd1= new BDLibrosDisponibles();
     JComboBox combo;//MOSTRAR DATOS EN UN CUADRO COMBINADO DESPLEGABLE
     //MENU DESPLEGABLE DE AÑADIR NUEVO LIBRO
      String [] nuevoLibro = {"","Ciencia-Ficcion","Misterio y Terror","Drama", "Aventuras", "Humor"};
      
      
      
//constructor de la ventana principal  
    public Proyecto() 
    {
        //INICIALIZACION DE LOS BOTONES
        panel = this.getContentPane(); //llamamos al contenedor de panel
        //LLAMAMOS AL PANEL SETLAYOUT NULL PARA DARLE VALORES A LOS BOTONES
        panel.setLayout(null);//asocia el panel al gestor de layout       
        estanteria1=new JButton ("Ciencia-Ficción");
        estanteria1.setBounds(50, 10, 300, 30);//tamaño y posición del botón
       
        estanteria2=new JButton("Misterio y Terror");
        estanteria2.setBounds(50,50,300, 30);

        estanteria3=new JButton("Drama");
        estanteria3.setBounds(50,100,300, 30);

        estanteria4=new JButton("Aventuras");
        estanteria4.setBounds(50,150,300, 30);

        estanteria5=new JButton("Humor");
        estanteria5.setBounds(50,200,300, 30);

        botonbuscar=new JButton("buscar");
        botonbuscar.setBounds(10,300,100, 30);
        botonbuscar.addActionListener(new OyenteBoton());//llama a la clase oyenteboton
        
        JLabel titulo1=new JLabel("Buscar por titulo");//titulo para el campo de texto
        titulo1.setBounds(10, 250, 100, 30);//tamaño y posicion del campo de texto
        panel.add(titulo1);
        titulo =new JTextField();
        titulo.setBounds(130,250,180, 30);  
        
        botonAñadir=new JButton("Nuevo libro");//titulo del botonañadir
        botonAñadir.setBounds(110,300,100, 30);
        botonAñadir.addActionListener(new OyenteBotonAñadir());//llama a la clase oyentebotonAÑADIR
        
        
        bimportar=new JButton("Importar");//titulo del boton importar
        bimportar.setBounds(220,300,100, 30);
        bimportar.addActionListener(new OyenteImportar());//llama a la clase oyentebotonImportar
         
        
        //LLAMAMOS A LA CLASE OYENTE BOTON CON LOS DISTINTOS BOTONES
        estanteria1.addActionListener(new oyenteCienciaFiccion());
        estanteria2.addActionListener(new oyenteMisterio());
        estanteria3.addActionListener(new oyenteDrama());
        estanteria4.addActionListener(new oyenteAventuras());
        estanteria5.addActionListener(new oyenteHumor());
        
        panel.add(titulo);//añadimos el campo de texto
        panel.add(estanteria1);//añadimos los botones a la ventana principal
        panel.add(estanteria2);
        panel.add(estanteria3);
        panel.add(estanteria4);
        panel.add(estanteria5);
        panel.add(botonbuscar);
        panel.add(botonAñadir);
        panel.add(bimportar);
        panel.setBackground(Color.YELLOW);//color de fondo
        setSize(400,400); //tamaño de la ventana perincipal
        setVisible(true);//para hacer visible la ventana
        setDefaultCloseOperation(EXIT_ON_CLOSE);//metodo que al pulsar la x de la ventana,cerramos la aplicación
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    //EN LA VENTANA QUE APARECE AL PULSAR NUEVOLIBRO,LE INDICO AL BOTON AÑADIR LO QUE TIENE QUE HACER
class OyenteBotonAñadir implements ActionListener {
        public void actionPerformed(ActionEvent e) 
        {
            
            //creamos ventana jframe
            JFrame jpanel= new JFrame("AÑADIR NUEVO LIBRO");
            JDesktopPane esc1= new JDesktopPane();//contenedor de los diferentes formularios del jframe
          
            
            JLabel titulo1=new JLabel("TITULO");//titulo para el campo de texto
            titulo1.setBounds(30, 50, 80, 25);//tamaño y posicion del campo de texto
            esc1.add(titulo1);
            
            tituloLi =new JTextField();//para escribir en el campo de texto
            tituloLi.setBounds(130,50,180, 25);  
            esc1.add(tituloLi);//añadimos el titulo al desktoppane
            
            JLabel titulo2=new JLabel("AUTOR");//titulo para el campo de texto
            titulo2.setBounds(30, 80, 80, 25);//tamaño y posicion del campo de texto
            esc1.add(titulo2);
            autor =new JTextField();
            autor.setBounds(130,80,180, 25);  
            esc1.add(autor);
            
            combo = new JComboBox(nuevoLibro);//muestra datos
            combo.setBounds(80, 130, 180, 25);
            esc1.add(combo);
            
            esc1.setBackground(Color.YELLOW);
            //acciones que realiza el boton nuevo
            bnuevo=new JButton("AÑADIR");//boton añadir
            bnuevo.setBounds(80,200,100, 30);
            esc1.add(bnuevo);
            bnuevo.addActionListener(new OyenteNuevoLibro());//llama a la clase oyenteboton
            
            
            jpanel.add(esc1);
            jpanel.setSize(400, 400);
            jpanel.setResizable(false);
            jpanel.setLocationRelativeTo(null);
            jpanel.setVisible(true);  
            
        }
        
   }
//clase del bnuevo
class OyenteNuevoLibro implements ActionListener {
        public void actionPerformed(ActionEvent e) 
        {
            String tituloL=tituloLi.getText();
            String autorl=autor.getText();
            String generol=(String) combo.getSelectedItem();
            System.out.println(tituloL+"-"+autorl+"--"+generol);
            
            try {
                bd1.añadirLibro(tituloL, generol, autorl);//añadir al objeto de la base de datos
                                                          //titulo,autor y genero nuevos
            } catch (SQLException ex) {
                Logger.getLogger(Proyecto.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
   }
//clase del bimportar
class OyenteImportar implements ActionListener {
        public void actionPerformed(ActionEvent e) 
        {
            selector=new JFileChooser();
            int seleccion = selector.showOpenDialog(null);
             if (seleccion == JFileChooser.APPROVE_OPTION)
             {
                 fichero = selector.getSelectedFile();
           
            
             try {  
                 
                 BufferedReader bfr= new BufferedReader(new FileReader(fichero));  

                 StringTokenizer st;
                 int i=0;
                 String linea, li, au, gen;

                 /*Lee el fichero linea a linea hasta llegar a la ultima*/  
                 while((linea=bfr.readLine())!=null) 
                 { 
                   
                     st= new StringTokenizer(linea, ",");
                     li=st.nextToken();
                     au=st.nextToken();
                     gen=st.nextToken();
                     bd1.importar(li, au, gen);
                     
                
                 }
                 JOptionPane.showMessageDialog(null, "IMPORTADO");
                 bfr.close();
                 
                 
             } catch (SQLException ex) {
                Logger.getLogger(Proyecto.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Proyecto.class.getName()).log(Level.SEVERE, null, ex);
            }
             }
                 
        }
        
   }

  /*clases del los distintos botones */
//ciencia ficcion
    class oyenteCienciaFiccion implements ActionListener {
        public void actionPerformed(ActionEvent e) 
        {
            //al pulsar sobre el boton aparece tabla nueva
            //con un menu de opciones entre ellas guardar y listado de libros
            //con dos columnas autor y titulo del libro
            DefaultTableModel modeloTabla;
            JMenu menuOpciones = new JMenu("Guardar");
            JMenuItem jmGuardar;
            jmb1 = new JMenuBar();
            jmb1.add(menuOpciones);
            menuOpciones.add(jmGuardar = new JMenuItem("Guardar", 'G'));
            jmGuardar.addActionListener(new OyenteGuardar());//llamo a la clase oyenteguardar para
                                                             //que guarde los libros que añadimos
            

            JFrame jpanel= new JFrame("Listado libros: Ciencia-Ficcion");
            BDLibrosDisponibles bd= new BDLibrosDisponibles();
            Vector columnas = new Vector();
            //creo 2 columnas que son las que devuelve la consulta, en la tabla
            columnas.add("Titulo");
            columnas.add("Autor");       
            
            modeloTabla = new DefaultTableModel(null, columnas);
            tabla = new JTable(modeloTabla);
            tabla.setOpaque(true);

            // Panel para la TABLA
            JScrollPane panel =new JScrollPane(tabla);
            jpanel.add(panel);
            jpanel.setBackground(Color.PINK);
            ArrayList<Object[]> datos = null;   
            try {
                datos = bd.listadoCienciaF();
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(Proyecto.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            // Vaciar el Listado anterior.
            int nfilas = modeloTabla.getRowCount();
            
            for (int i=0; i<nfilas; i++) {
                modeloTabla.removeRow(0);
            }
            // Añadir el Listado actual
            for (int i=0; i<datos.size(); i++) {
                modeloTabla.addRow(datos.get(i));
            }
            
        
            
            jpanel.setJMenuBar(jmb1);
            jpanel.setSize(800, 600);
            jpanel.setResizable(false);
            jpanel.setLocationRelativeTo(null);
            jpanel.setVisible(true);  
            
        }
   }
    class oyenteDrama implements ActionListener {
        public void actionPerformed(ActionEvent e) 
        {
            
            DefaultTableModel modeloTabla;
            JMenu menuOpciones = new JMenu("Guardar");
            JMenuItem jmGuardar;
            jmb1 = new JMenuBar();
            jmb1.add(menuOpciones);
            menuOpciones.add(jmGuardar = new JMenuItem("Guardar", 'G'));
            jmGuardar.addActionListener(new OyenteGuardar());

            JFrame jpanel= new JFrame("Listado libros: Drama");
            BDLibrosDisponibles bd= new BDLibrosDisponibles();
            Vector columnas = new Vector();
            //creo 2 columnas que son las que devuelve la consulta, en la tabla
            columnas.add("Titulo");
            columnas.add("Autor");       
            
            modeloTabla = new DefaultTableModel(null, columnas);
            tabla = new JTable(modeloTabla);
            tabla.setOpaque(true);

            // Panel para la TABLA
            JScrollPane panel =new JScrollPane(tabla);
            jpanel.add(panel);
            jpanel.setBackground(Color.PINK);
            ArrayList<Object[]> datos = null;   
            try {
                datos = bd.listadoDrama();
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(Proyecto.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            // Vaciar el Listado anterior.
            int nfilas = modeloTabla.getRowCount();
            
            for (int i=0; i<nfilas; i++) {
                modeloTabla.removeRow(0);
            }
            // Añadir el Listado actual
            for (int i=0; i<datos.size(); i++) {
                modeloTabla.addRow(datos.get(i));
            }
             
            jpanel.setJMenuBar(jmb1);
            jpanel.setSize(800, 600);
            jpanel.setResizable(false);
            jpanel.setLocationRelativeTo(null);
            jpanel.setVisible(true);  
            
        }
   }
    class oyenteAventuras implements ActionListener {
        public void actionPerformed(ActionEvent e) 
        {
            
            DefaultTableModel modeloTabla;
            JMenu menuOpciones = new JMenu("Guardar");
            JMenuItem jmGuardar;
            jmb1 = new JMenuBar();
            jmb1.add(menuOpciones);
            menuOpciones.add(jmGuardar = new JMenuItem("Guardar", 'G'));
            jmGuardar.addActionListener(new OyenteGuardar());

            JFrame jpanel= new JFrame("Listado libros: Aventuras");
            BDLibrosDisponibles bd= new BDLibrosDisponibles();
            Vector columnas = new Vector();
            //creo 2 columnas que son las que devuelve la consulta, en la tabla
            columnas.add("Titulo");
            columnas.add("Autor");       
            
            modeloTabla = new DefaultTableModel(null, columnas);
            tabla = new JTable(modeloTabla);
            tabla.setOpaque(true);

            // Panel para la TABLA
            JScrollPane panel =new JScrollPane(tabla);
            jpanel.add(panel);
            jpanel.setBackground(Color.PINK);
            ArrayList<Object[]> datos = null;   
            try {
                datos = bd.listadoAventuras();
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(Proyecto.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            // Vaciar el Listado anterior.
            int nfilas = modeloTabla.getRowCount();
            
            for (int i=0; i<nfilas; i++) {
                modeloTabla.removeRow(0);
            }
            // Añadir el Listado actual
            for (int i=0; i<datos.size(); i++) {
                modeloTabla.addRow(datos.get(i));
            }
           
            
            
            jpanel.setJMenuBar(jmb1);
            jpanel.setSize(800, 600);
            jpanel.setResizable(false);
            jpanel.setLocationRelativeTo(null);
            jpanel.setVisible(true);  
            
        }
   }
public static void main(String args[])throws IOException{
    Proyecto ventana =new Proyecto();  
  
    
    
    
}

class oyenteMisterio implements ActionListener {
        public void actionPerformed(ActionEvent e) 
        {
            
            DefaultTableModel modeloTabla;
            JMenu menuOpciones = new JMenu("Guardar");
            JMenuItem jmGuardar;
            jmb1 = new JMenuBar();
            jmb1.add(menuOpciones);
            menuOpciones.add(jmGuardar = new JMenuItem("Guardar", 'G'));
            jmGuardar.addActionListener(new OyenteGuardar());

            JFrame jpanel= new JFrame("Listado libros: Misterio y Terror");
            BDLibrosDisponibles bd= new BDLibrosDisponibles();
            Vector columnas = new Vector();
            //creo 2 columnas que son las que devuelve la consulta, en la tabla
            columnas.add("Titulo");
            columnas.add("Autor");       
            
            modeloTabla = new DefaultTableModel(null, columnas);
            tabla = new JTable(modeloTabla);
            tabla.setOpaque(true);

            // Panel para la TABLA
            JScrollPane panel =new JScrollPane(tabla);
            jpanel.add(panel);
            jpanel.setBackground(Color.PINK);
            ArrayList<Object[]> datos = null;   
            try {
                datos = bd.listadoMisterio();
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(Proyecto.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            // Vaciar el Listado anterior.
            int nfilas = modeloTabla.getRowCount();
            
            for (int i=0; i<nfilas; i++) {
                modeloTabla.removeRow(0);
            }
            // Añadir el Listado actual
            for (int i=0; i<datos.size(); i++) {
                modeloTabla.addRow(datos.get(i));
            }
            
            jpanel.setJMenuBar(jmb1);
            jpanel.setSize(800, 600);
            jpanel.setResizable(false);
            jpanel.setLocationRelativeTo(null);
            jpanel.setVisible(true);  
            
        }
   }
class oyenteHumor implements ActionListener {
        public void actionPerformed(ActionEvent e) 
        {
            
            DefaultTableModel modeloTabla;
            JMenu menuOpciones = new JMenu("Guardar");
            JMenuItem jmGuardar;
            jmb1 = new JMenuBar();
            jmb1.add(menuOpciones);
            menuOpciones.add(jmGuardar = new JMenuItem("Guardar", 'G'));
            jmGuardar.addActionListener(new OyenteGuardar());

            JFrame jpanel= new JFrame("Listado libros: Humor");
            BDLibrosDisponibles bd= new BDLibrosDisponibles();
            Vector columnas = new Vector();
            //creo 2 columnas que son las que devuelve la consulta, en la tabla
            columnas.add("Titulo");
            columnas.add("Autor");       
            
            modeloTabla = new DefaultTableModel(null, columnas);
            tabla = new JTable(modeloTabla);
            tabla.setOpaque(true);

            // Panel para la TABLA
            JScrollPane panel =new JScrollPane(tabla);
            jpanel.add(panel);
            jpanel.setBackground(Color.PINK);
            ArrayList<Object[]> datos = null;   
            try {
                datos = bd.listadoHumor();
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(Proyecto.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            // Vaciar el Listado anterior.
            int nfilas = modeloTabla.getRowCount();
            
            for (int i=0; i<nfilas; i++) {
                modeloTabla.removeRow(0);
            }
            // Añadir el Listado actual
            for (int i=0; i<datos.size(); i++) {
                modeloTabla.addRow(datos.get(i));
            }
            
            jpanel.setJMenuBar(jmb1);
            jpanel.setSize(800, 600);
            jpanel.setResizable(false);
            jpanel.setLocationRelativeTo(null);
            jpanel.setVisible(true);  
            
        }
   }
//clase del botonguardar
class OyenteGuardar implements ActionListener {
    public void actionPerformed(ActionEvent e) { 
        selector=new JFileChooser();//clase que nos permite mostrar una ventana para seleccioner fichero

        
        int seleccion = selector.showSaveDialog(null);
        if (seleccion == JFileChooser.APPROVE_OPTION)
        {
            fichero = selector.getSelectedFile();
            PrintWriter fEscritura = null;
            try {
            fEscritura = new PrintWriter(new FileWriter(fichero)); // CREADO FLUJO DE ESCRITURA y que no sobreescriba
            } catch (IOException ex) { 
                Logger.getLogger(Proyecto.class.getName()).log(Level.SEVERE, null, ex);
            } 
            int y = 0;
            int x=0;
            for( y=0; y<=tabla.getRowCount()-1;y++ ) 
            {// devuelve nº de filas
                for(x=0; x<=1; x++ ) { // nº de columnas son 2 
                    fEscritura.print(tabla.getValueAt(y, x)+", "); 
                }
                fEscritura.print("\r\n");
                // out.println();
            }
            JOptionPane.showMessageDialog(null,"Salir");
            fEscritura.flush();//vacia el buffer
            fEscritura.close();//termina de escribir
        }
       
    }
}


//clase que me describe lo que tiene que hacer el botonbuscar
class OyenteBoton implements ActionListener{
    
    public void actionPerformed(ActionEvent ae){
       
       
        String buscar=titulo.getText();
        BDLibrosDisponibles bd=new BDLibrosDisponibles();//objeto de la clase BDLibrosDisponibles
        
        /*ArrayList de objetos de la base de datos libreria,llamamos al metodo realizar consulta*/
        //consulta sql que busca el titulo del libro de nuestra busqueda en la tabla libros de la base de datos Libreria
        ArrayList<Object[]>datos=bd.realizarConsulta("select * from libros where Titulo="+"'"+buscar+"'");
        
        if(datos.size()<=0){//si el arraylist datos no tiene ningun valor,la consulta la devuelve vacia
            JOptionPane.showMessageDialog(null, "Libro no disponible");//muestra una ventana de alerta con un mensaje libro no disponible
            
}
    }
}


}



